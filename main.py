import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import csv
from datetime import datetime

def calculate_fan_on_time(file_path):
    # Read the CSV file into a pandas DataFrame
    df = pd.read_csv(file_path, parse_dates=['Timestamp'])

    # Calculate the percentage of time the fan has been on
    total_time = df['Timestamp'].iloc[-1] - df['Timestamp'].iloc[0]

    # Open the CSV file
    with open(file_path, 'r') as file:
        # Create a CSV reader
        reader = csv.reader(file)

        # Skip the header row
        next(reader)

        # Initialize variables
        fan_on_time = 0
        start_time = None

        # Iterate through each row in the CSV
        for row in reader:
            timestamp_str, value_str = row
            timestamp = datetime.strptime(timestamp_str, '%Y-%m-%d %H:%M:%S')
            value = int(value_str)

            # Check if the current value is 1
            if value == 1:
                # Check if this is the first occurrence of value 1
                if start_time is None:
                    start_time = timestamp
                last_one = timestamp
            else:
                # If the value is 0, calculate the time difference and add to fan_on_time
                if start_time is not None:
                    time_difference = last_one - start_time
                    fan_on_time += time_difference.total_seconds()
                    start_time = None

    return df, total_time, fan_on_time

def plot_data(df, fan_percentage):
    # Plotting
    plt.figure(figsize=(10, 6))
    plt.plot(df['Timestamp'], df['Value'], marker='', linestyle='-', color='b')
    plt.title('Status der Badlüftung über die Zeit')
    plt.xlabel('Zeitpunkt [UTC]')
    plt.ylabel('Status der Lüftung [0=AUS, 1=AN]')
    plt.grid(True)

    comment_indices = [25125, 25375]  # Indices of data points to annotate
    comment_texts = ['Anfang Duschvorgang', 'Ende Duschvorgang']

    for index, text in zip(comment_indices, comment_texts):
        plt.annotate(text, xy=(df['Timestamp'][index], df['Value'][index]),
                     xytext=(df['Timestamp'][index], df['Value'][index] + 0.1),
                     arrowprops=dict(facecolor='black', arrowstyle='wedge,tail_width=0.7'),
                     fontsize=8, ha='right' if text.startswith('Anfang') else 'left')

    # Add a comment on the top left indicating the percentage of time the fan has been on
    annotation_text = f'Lüfter AN: {fan_percentage:.0f}% der Gesamtzeit'
    plt.annotate(annotation_text, xy=(0.01, 0.95), xycoords='axes fraction', fontsize=10, ha='left', va='top')

    # Format x-axis as German time
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%d.%m. %H:%m'))

    plt.ylim(0, 1.2)
    plt.show()

def main():
    file_path = 'data.csv'  # Update this with the actual file path
    df, total_time, fan_on_time = calculate_fan_on_time(file_path)
    fan_percentage = (fan_on_time / total_time.total_seconds()) * 100
    print(f'Total seconds with status 1: {fan_on_time}')
    plot_data(df, fan_percentage)

if __name__ == "__main__":
    main()
