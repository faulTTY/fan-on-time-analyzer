import pandas as pd
import matplotlib.pyplot as plt

# Read the CSV file into a pandas DataFrame
df = pd.read_csv('data.csv', parse_dates=['Timestamp'])

# Create a figure and three subplots
fig, (ax1, ax2, ax3) = plt.subplots(3, 1, sharex=True, figsize=(10, 8))

# Plot Loudness
ax1.plot(df['Timestamp'], df['Loudness'], label='Fan', color='green')
ax1.set_ylabel('Fan')
ax1.legend()
ax1.grid(True)

# Plot Humidity
ax2.plot(df['Timestamp'], df['Humidity [%]'], label='Humidity', color='blue')
ax2.axhline(y=55, color='lightsteelblue', linestyle='--', label='Reference value + 7% = 55%')
ax2.axhline(y=48, color='indigo', linestyle='--', label='Lowest reference value 48%')
ax2.set_ylabel('Humidity [%]')
ax2.legend()
ax2.grid(True)

# Plot Temperature
ax3.plot(df['Timestamp'], df['Temperature [C]'], label='Temperature', color='red')
ax3.set_xlabel('Timestamp')
ax3.set_ylabel('Temperature [C]')
ax3.legend()
ax3.grid(True)

# Set title for the entire plot
plt.suptitle('Loudness, Humidity, and Temperature Over Time', y=1.02)

# Show the plot
plt.show()
