# Fan On-Time Analyzer
## Overview

This Python script analyzes the operational status of a fan over a period of time based on a provided CSV file containing timestamped status data. The script calculates the percentage of time the fan has been in the "on" state and visualizes the fan's status over time using matplotlib.
Prerequisites

## Ensure you have the following libraries installed:

- pandas
- matplotlib

## Install them using:

```bash
pip install pandas matplotlib
```

## Usage

Update the file_path variable in the main function with the actual path to your CSV file.

```python
file_path = 'data.csv'  # Update this with the actual file path
```

Run the script:
```bash
python script_name.py
```

## Functionality

`calculate_fan_on_time(file_path)`

- Reads the CSV file into a pandas DataFrame.
- Calculates the total time span covered by the data.
- Iterates through the data to calculate the total time the fan has been in the "on" state.

`plot_data(df, fan_percentage)`
- Plots the fan's status over time.
- Annotates specific data points with comments (e.g., start and end of a shower).
- Adds an annotation indicating the percentage of time the fan has been on.

`main()`
- Calls `calculate_fan_on_time` to obtain relevant data.
- Calculates the fan's percentage on-time.
- Prints the total time the fan has been on.
- Calls plot_data to visualize the fan's status over time.

## main3.py

An additional script has been added to plot the data of two sensors together
- The microphone
- A DHT11 humidity and temperature sensor

The `data.csv` has the following structure:

```csv
Timestamp,Loudness,Humidity [%],Temperature [C]
2023-11-25 07:51:14,0,25.0,20.6
2023-11-25 07:51:17,0,25.0,20.6
```

## Example

### Example microphone only
The script includes an example using a CSV file named `example_data.csv`. Replace this with your own file path for analysis.

![example](example.png)


### Example with DHT11

![example_dht11](example_dht11.png)

## Note
- The script assumes that the CSV file contains two columns: 'Timestamp' and 'Value', where 'Value' represents the status of the fan (0 for off, 1 for on).
- The script `main3.py` assumes 4 columns: `Timestamp,Loudness,Humidity [%],Temperature [C]`
- The time format in the CSV file should be '%Y-%m-%d %H:%M:%S'.

Feel free to customize the script based on your specific use case and data format.